import ballerina/io;
import ballerina/http;


type L record {
    string Lecture_Name;
    string Lecture_info;
    byte Signature;
    string courseCode;
    string courseContent;
};

http:Client httpClient=check new("https://CourseOutline/jsonFile.json");

service / on new http:Listener(8080) {

    public function lectureCourseCreate() returns error?? {
    // Initializes the JSON file path and content.
    string jsonFilePath = "./CourseOutline/jsonFile.json";
    json jsonContent = {"CourseOutline": {
            "course": " ",
            "Course_Code": " ",
            "Lecture_Name": " ",
            "Course_Discription": " ",
            "Learning_Outcomes": " ",
            "Assesments": " ", 
            "Lecture_info": { 
                "Contacts": " ",
                "LectureName": "  ",
                "Office_Number": "  "
            },
            "Working_hours": " ",
            "Signature": " "
      }};

    // Writes the given JSON to a file.
    check io:fileWriteJson(jsonFilePath, jsonContent);
    // If the write operation was successful, then, performs a read operation to read the JSON content.
    json readJson = check io:fileReadJson(jsonFilePath);
    io:println(readJson);
    }
}