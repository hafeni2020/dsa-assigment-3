import ballerina/http;
import ballerinax/kubernetes;
import ballerina/log;


@kubernetes:Service {
    name: "course-outline"

}

type Lecture  record {
    string Lecture_name;
    string Lecture_info;
    byte Signature;
    string course_code;
};

http:Client